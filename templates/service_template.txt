import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Condition } from '../../basis/condition';
import { ApiService } from '../../basis/services/api.service';
import { {{model.name_upper_camel}} } from '../../models/{{model.name_kebab}}';
import { BaseBizService } from '../../basis/services/base-biz.service';

@Injectable({
    providedIn: 'root'
})
/**
 * {{model.description}}サービス。
 */
export class {{model.name_upper_camel}}Service extends BaseBizService<{{model.name_upper_camel}}> {

    /**
     * {{model.name_upper_camel}}Serviceのインスタンスを生成します。
     */
    constructor(api: ApiService) {
        super(api);
    }

    public search(condition: Condition): Observable<{{model.name_upper_camel}}[]> {
        return this.api.call<any[]>('search_issues', condition).pipe<{{model.name_upper_camel}}[]>(
            map(
                res => {
                    let results: {{model.name_upper_camel}}[] = [];
                    res.forEach(
                        item => {
                            results.push(new {{model.name_upper_camel}}().fromJson(item));
                        }
                    );
                    return results;
                }
            )
        );
    }

    public get(id: number): Observable<{{model.name_upper_camel}}> {
        let model = new {{model.name_upper_camel}}();
        model.id = id;
        return this.api.call<any>('get_{{model.name_snake}}', model).pipe<Issue>(
            map(
                res => {
                    return new {{model.name_upper_camel}}().fromJson(res);
                }
            )
        );
    }

    public save(model: {{model.name_upper_camel}}): Observable<{{model.name_upper_camel}}> {
        return this.api.call<any>('save_{{model.name_snake}}', model).pipe<Issue>(
            map(
                res => {
                    let result = new {{model.name_upper_camel}}();
                    return result.fromJson(res);
                }
            )
        );
    }

    public delete(model: {{model.name_upper_camel}}): Observable<{{model.name_upper_camel}}> {
        return this.api.call<any>('save_{{model.name_snake}}', model).pipe<Issue>(
            map(
                res => {
                    let result = new {{model.name_upper_camel}}();
                    return result.fromJson(res);
                }
            )
        );
    }

    public build(): Observable<{{model.name_upper_camel}}> {
        return this.api.call<any>('init_{{model.name_snake}}', null).pipe<{{model.name_upper_camel}}>(
            map(
                res => {
                    let result = new {{model.name_upper_camel}}();
                    return result.fromJson(res);
                }
            )
        );
    }

}
