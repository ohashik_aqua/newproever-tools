'''
言語ファイルを生成するツール。
'''
import codecs
import logging
import os

import xlrd
from jinja2 import Environment, FileSystemLoader

LOGGER = logging.getLogger(__name__)

class Text:
    '''
    テキストのキーと値を保持します。
    '''
    key = ''

    value = ''

    def __init__(self, key, value):
        self.key = key
        self.value = value

LOCALES = ['ja', 'en', 'zh-cn', 'zh-tw']

PREFIXES = [
    ('メッセージ', 'message'), ('画面', 'screen'), ('データ項目', 'attribute'), ('メニュー', 'menu'), ('検索条件', 'condition')
]

def read_definition():
    '''
    定義ファイルを読み込みます。
    '''
    src_path = os.path.dirname(__file__)
    doc_path = os.path.join(src_path, 'doc')
    name_map = {}
    book = xlrd.open_workbook(os.path.join(doc_path, '文言キー.xlsx'))
    for sheet_index in range(book.nsheets):
        sheet = book.sheet_by_index(sheet_index)
        for row in range(1, sheet.nrows):
            name = sheet.cell(row, 0).value
            if not name:
                break
            if name in name_map:
                LOGGER.warning('duplicated name. %s', name)
                continue
            name_map[name] = sheet.cell(row, 1).value
    LOGGER.debug(name_map)

    texts = [[] for locale in LOCALES]
    texts_path = os.path.join(doc_path, '文言')
    for text_file in os.listdir(texts_path):
        LOGGER.debug(text_file)
        if not text_file.endswith('.xlsx') or text_file.startswith('~'):
            continue
        book = xlrd.open_workbook(os.path.join(texts_path, text_file))
        for sheet_index in range(book.nsheets):
            sheet = book.sheet_by_index(sheet_index)
            key_prefix = ''
            for prefix in PREFIXES:
                if sheet.name.startswith(prefix[0]):
                    key_prefix = prefix[1]
                    break
            if not key_prefix:
                LOGGER.debug('skip sheet.%s', sheet.name)
                continue
            LOGGER.debug(key_prefix)
            for row in range(1, sheet.nrows):
                key = key_prefix
                seq = sheet.cell(row, 0).value
                if not seq:
                    break
                for col in range(1, 4):
                    name = sheet.cell(row, col).value
                    if not name:
                        continue
                    if name in name_map:
                        key += '.' + name_map[name]
                for col in range(4, 6):
                    code = sheet.cell(row, col).value
                    if not code:
                        continue
                    key += '.' + code
                index = 0
                for col in range(6, 10):
                    value = sheet.cell(row, col).value
                    if not value:
                        continue
                    texts[index].append(Text(key, value))
                    index += 1
    return texts

def generate(texts):
    '''
    ソースコードを生成します。
    '''
    src_path = os.path.dirname(__file__)
    dst_path = os.path.join(src_path, 'dst/locales')
    LOGGER.debug('dst path:%s', dst_path)
    for dstf in os.listdir(dst_path):
        if os.path.isdir(os.path.join(dst_path, dstf)):
            continue
        os.remove(os.path.join(dst_path, dstf))
    env = Environment(loader=FileSystemLoader(os.path.join(src_path, 'templates'), encoding='utf-8'))
    template = env.get_template('local_template.txt')
    for index, text in enumerate(texts):
        body = template.render({'texts': text})
        file_name = 'locale-' + LOCALES[index] + '.json'
        with codecs.open(os.path.join(dst_path, file_name), 'w', encoding='utf-8') as dst:
            dst.write(body)

def main():
    '''
    定義ファイルを読み込み、文言ファイルを生成します。
    '''
    texts = read_definition()
    LOGGER.debug('texts.%s', len(texts))
    generate(texts)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    LOGGER.info('called.')
    main()
