
import { Model } from "../basis/model";
import { WorkItem } from './work-item';
import { HumanResource } from './human-resource';

/**
 * コミュニケーション計画を表します。
 */
export class CommunicationPlan implements Model {

    public static COLUMNS = ['communication_plan_id','communication_plan_project_id','communication_plan_number','communication_plan_stakeholder_id','communication_plan_importance','communication_plan_currently_interesting','communication_plan_ideal_interesting','communication_plan_transfer_contents','communication_plan_timing','communication_plan_communication_frequency','communication_plan_communication_method','communication_plan_responsible_person_human_resources',];

    /**
     * コミュニケーション計画ID
     */
    public id: number;

    /**
     * コミュニケーション計画プロジェクトID
     */
    public projectId: number;

    /**
     * コミュニケーション計画番号
     */
    public number: string;

    /**
     * コミュニケーション計画ワークアイテム
     */
    public workItem: WorkItem;

    /**
     * コミュニケーション計画ステークホルダーID
     */
    public stakeholderId: number;

    /**
     * コミュニケーション計画重要度
     */
    public importance: string;

    /**
     * コミュニケーション計画現在関心度
     */
    public currentlyInteresting: string;

    /**
     * コミュニケーション計画理想関心度
     */
    public idealInteresting: string;

    /**
     * コミュニケーション計画伝達内容
     */
    public transferContents: string;

    /**
     * コミュニケーション計画タイミング
     */
    public timing: string;

    /**
     * コミュニケーション計画コミュニケーション頻度
     */
    public communicationFrequency: string;

    /**
     * コミュニケーション計画コミュニケーション方法
     */
    public communicationMethod: string;

    /**
     * コミュニケーション計画実施責任者ヒューマンリソース
     */
    public responsiblePersonHumanResources: HumanResource;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): CommunicationPlan {
        if (!json) {
            return this;
        }
        this.id = json['communication_plan_id'];
        this.projectId = json['communication_plan_project_id'];
        this.number = json['communication_plan_number'];
        this.workItem = new WorkItem().fromJson(json['communication_plan_work_item']);
        this.stakeholderId = json['communication_plan_stakeholder_id'];
        this.importance = json['communication_plan_importance'];
        this.currentlyInteresting = json['communication_plan_currently_interesting'];
        this.idealInteresting = json['communication_plan_ideal_interesting'];
        this.transferContents = json['communication_plan_transfer_contents'];
        this.timing = json['communication_plan_timing'];
        this.communicationFrequency = json['communication_plan_communication_frequency'];
        this.communicationMethod = json['communication_plan_communication_method'];
        this.responsiblePersonHumanResources = new HumanResource().fromJson(json['communication_plan_responsible_person_human_resources']);
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['communication_plan_id'] = this.id;
        json['communication_plan_project_id'] = this.projectId;
        json['communication_plan_number'] = this.number;
        json['communication_plan_work_item'] = this.workItem ? this.workItem.toJson() : {};
        json['communication_plan_stakeholder_id'] = this.stakeholderId;
        json['communication_plan_importance'] = this.importance;
        json['communication_plan_currently_interesting'] = this.currentlyInteresting;
        json['communication_plan_ideal_interesting'] = this.idealInteresting;
        json['communication_plan_transfer_contents'] = this.transferContents;
        json['communication_plan_timing'] = this.timing;
        json['communication_plan_communication_frequency'] = this.communicationFrequency;
        json['communication_plan_communication_method'] = this.communicationMethod;
        json['communication_plan_responsible_person_human_resources'] = this.responsiblePersonHumanResources ? this.responsiblePersonHumanResources.toJson() : {};
        return json;
    }
}