
import { Model } from "../basis/model";

/**
 * ヒューマンリリソースを表します。
 */
export class HumanResource implements Model {

    public static COLUMNS = ['human_resources_id','human_resources_name',];

    /**
     * ヒューマンリソースID
     */
    public id: number;

    /**
     * ヒューマンリソース名
     */
    public name: string;

    /**
     * 削除
     */
    public deleted: boolean;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): HumanResource {
        if (!json) {
            return this;
        }
        this.id = json['human_resources_id'];
        this.name = json['human_resources_name'];
        this.deleted = json['deleted'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['human_resources_id'] = this.id;
        json['human_resources_name'] = this.name;
        json['deleted'] = this.deleted;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.name = null;
        this.deleted = null;
    }

}