
import { Model } from "../basis/model";

/**
 * ユーザーを表します。
 */
export class User implements Model {

    public static COLUMNS = ['user_id','user_password',];

    /**
     * ユーザーID
     */
    public id: string;

    /**
     * ユーザーパスワード
     */
    public password: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): User {
        if (!json) {
            return this;
        }
        this.id = json['user_id'];
        this.password = json['user_password'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['user_id'] = this.id;
        json['user_password'] = this.password;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.password = null;
    }

}