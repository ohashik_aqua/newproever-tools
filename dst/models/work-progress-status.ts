
import { Model } from "../basis/model";
import { Issue } from './issue';
import { Task } from './task';
import { CommunicationPlan } from './communication-plan';

/**
 * 担当者進捗状況を表します。
 */
export class WorkProgressStatus implements Model {

    public static COLUMNS = ['issue','task','communication_plan',];

    /**
     * ワークアイテム種別
     */
    public workItemType: number;

    /**
     * 課題
     */
    public issue: Issue;

    /**
     * タスク
     */
    public task: Task;

    /**
     * コミュニケーション計画
     */
    public communicationPlan: CommunicationPlan;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): WorkProgressStatus {
        if (!json) {
            return this;
        }
        this.workItemType = json['work_item_type'];
        this.issue = new Issue().fromJson(json['issue']);
        this.task = new Task().fromJson(json['task']);
        this.communicationPlan = new CommunicationPlan().fromJson(json['communication_plan']);
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['work_item_type'] = this.workItemType;
        json['issue'] = this.issue ? this.issue.toJson() : {};
        json['task'] = this.task ? this.task.toJson() : {};
        json['communication_plan'] = this.communicationPlan ? this.communicationPlan.toJson() : {};
        return json;
    }
}