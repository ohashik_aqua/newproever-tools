
import { Model } from "../basis/model";

/**
 * フェーズを表します。
 */
export class Phase implements Model {

    public static COLUMNS = ['phase_id','phase_name',];

    /**
     * フェーズID
     */
    public id: number;

    /**
     * フェーズ名
     */
    public name: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Phase {
        if (!json) {
            return this;
        }
        this.id = json['phase_id'];
        this.name = json['phase_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['phase_id'] = this.id;
        json['phase_name'] = this.name;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.name = null;
    }

}