
import { ConditionModel } from "../basis/condition-model";

/**
 * 担当者進捗状況照会条件を表します。
 */
export class WorkProgressStatusCondition extends ConditionModel {

    public static COLUMNS = ['human_resources_id','human_resources_name','progress_status_id','progress_status_name','task_start_plan_date','task_end_plan_date',];

    /**
     * ヒューマンリソースID
     */
    public humanResourcesId: number;

    /**
     * ヒューマンリソース名
     */
    public humanResourcesName: string;

    /**
     * 進捗ステータスID
     */
    public progressStatusId: number;

    /**
     * 進捗ステータス名
     */
    public progressStatusName: string;

    /**
     * タスク開始予定日
     */
    public taskStartPlanDate: Date;

    /**
     * タスク終了予定日
     */
    public taskEndPlanDate: Date;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): WorkProgressStatusCondition {
        if (!json) {
            return this;
        }
        this.from = json['from'];
        this.to = json['to'];
        this.humanResourcesId = json['human_resources_id'];
        this.humanResourcesName = json['human_resources_name'];
        this.progressStatusId = json['progress_status_id'];
        this.progressStatusName = json['progress_status_name'];
        this.taskStartPlanDate = json['task_start_plan_date'];
        this.taskEndPlanDate = json['task_end_plan_date'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['from'] = this.from;
        json['to'] = this.to;
        json['human_resources_id'] = this.humanResourcesId;
        json['human_resources_name'] = this.humanResourcesName;
        json['progress_status_id'] = this.progressStatusId;
        json['progress_status_name'] = this.progressStatusName;
        json['task_start_plan_date'] = this.taskStartPlanDate;
        json['task_end_plan_date'] = this.taskEndPlanDate;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.humanResourcesId = null;
        this.humanResourcesName = null;
        this.progressStatusId = null;
        this.progressStatusName = null;
        this.taskStartPlanDate = null;
        this.taskEndPlanDate = null;
    }

}