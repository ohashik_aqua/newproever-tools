
import { ConditionModel } from "../basis/condition-model";

/**
 * ヒューマンリソース検索条件を表します。
 */
export class HumanResourceCondition extends ConditionModel {

    public static COLUMNS = ['human_resources_id','human_resources_name',];

    /**
     * ヒューマンリソースID
     */
    public humanResourcesId: number;

    /**
     * ヒューマンリソース名
     */
    public humanResourcesName: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): HumanResourceCondition {
        if (!json) {
            return this;
        }
        this.from = json['from'];
        this.to = json['to'];
        this.humanResourcesId = json['human_resources_id'];
        this.humanResourcesName = json['human_resources_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['from'] = this.from;
        json['to'] = this.to;
        json['human_resources_id'] = this.humanResourcesId;
        json['human_resources_name'] = this.humanResourcesName;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.humanResourcesId = null;
        this.humanResourcesName = null;
    }

}