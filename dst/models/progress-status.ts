
import { Model } from "../basis/model";

/**
 * 進捗ステータスを表します。
 */
export class ProgressStatus implements Model {

    public static COLUMNS = ['progress_status_id','progress_status_project_id','progress_status_number','progress_status_name','progress_status_progress_rate','progress_status_remarks','progress_status_default_flag',];

    /**
     * 進捗ステータスID
     */
    public id: number;

    /**
     * 進捗ステータスプロジェクトID
     */
    public projectId: number;

    /**
     * 進捗ステータス番号
     */
    public number: number;

    /**
     * 進捗ステータス名
     */
    public name: string;

    /**
     * 進捗ステータス進捗率
     */
    public progressRate: number;

    /**
     * 進捗ステータス備考
     */
    public remarks: string;

    /**
     * 進捗ステータスデフォルトフラグ
     */
    public defaultFlag: boolean;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): ProgressStatus {
        if (!json) {
            return this;
        }
        this.id = json['progress_status_id'];
        this.projectId = json['progress_status_project_id'];
        this.number = json['progress_status_number'];
        this.name = json['progress_status_name'];
        this.progressRate = json['progress_status_progress_rate'];
        this.remarks = json['progress_status_remarks'];
        this.defaultFlag = json['progress_status_default_flag'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['progress_status_id'] = this.id;
        json['progress_status_project_id'] = this.projectId;
        json['progress_status_number'] = this.number;
        json['progress_status_name'] = this.name;
        json['progress_status_progress_rate'] = this.progressRate;
        json['progress_status_remarks'] = this.remarks;
        json['progress_status_default_flag'] = this.defaultFlag;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.taskId = null;
        this.taskScopeId = null;
        this.taskNumber = null;
        this.taskWorkItemId = null;
        this.taskWbsLevel1 = null;
        this.taskWbsLevel2 = null;
        this.taskWbsLevel3 = null;
        this.taskWbsLevel4 = null;
        this.taskWbsLevel5 = null;
        this.taskStartPlanDate = null;
        this.taskEndPlanDate = null;
        this.taskPlanEffort = null;
        this.taskProgressRate = null;
        this.taskProgressStatusId = null;
        this.taskActualEffort = null;
        this.workItemStartImplementationDate = null;
        this.workItemEndImplementationDate = null;
        this.workItemOwnerHumanResourcesId = null;
        this.workItemType = null;
    }

}