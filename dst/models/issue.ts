
import { Model } from "../basis/model";
import { IssueUrgency } from './issue-urgency';
import { IssueStatus } from './issue-status';
import { Phase } from './phase';
import { HumanResource } from './human-resource';
import { OptionItem } from './option-item';
import { WorkItem } from './work-item';
import { Attachment } from './attachment';

/**
 * 課題を表します。
 */
export class Issue implements Model {

    public static COLUMNS = ['issue_id','issue_registration_date','issue_number','issue_subject','issue_detail','issue_correspondence_plan_date','issue_correspondence_date','issue_correspondence_detail','issue_completion_date','issue_exclusion_number','issue_issue_urgency','issue_issue_status','issue_occurence_phase','issue_tosolve_phase','issue_registered_person_human_resource','issue_corresponding_person_human_resource','issue_option_items','issue_related_items','issue_attachments','issue_watcher_human_resources',];

    /**
     * 課題ID
     */
    public id: number;

    /**
     * 課題登録日
     */
    public registrationDate: Date;

    /**
     * 課題番号
     */
    public number: string;

    /**
     * 課題件名
     */
    public subject: string;

    /**
     * 課題詳細
     */
    public detail: string;

    /**
     * 課題対応予定日
     */
    public correspondencePlanDate: Date;

    /**
     * 課題対応日
     */
    public correspondenceDate: Date;

    /**
     * 課題対応詳細
     */
    public correspondenceDetail: string;

    /**
     * 課題完了日
     */
    public completionDate: Date;

    /**
     * 課題排他番号
     */
    public exclusionNumber: number;

    /**
     * 課題緊急度
     */
    public issueUrgency: IssueUrgency;

    /**
     * 課題ステータス
     */
    public issueStatus: IssueStatus;

    /**
     * 課題発生フェーズ
     */
    public occurencePhase: Phase;

    /**
     * 課題解決すべきフェーズ
     */
    public tosolvePhase: Phase;

    /**
     * 課題登録ヒューマンリソース
     */
    public registeredPersonHumanResource: HumanResource;

    /**
     * 課題対応者ヒューマンリソース
     */
    public correspondingPersonHumanResource: HumanResource;

    /**
     * 課題オプション項目
     */
    public optionItems: OptionItem[];

    /**
     * 課題関連情報
     */
    public relatedItems: WorkItem[];

    /**
     * 課題添付ファイル
     */
    public attachments: Attachment[];

    /**
     * 課題ウォッチャーヒューマンリソース
     */
    public watcherHumanResources: HumanResource[];

    /**
     * 課題完了可能フラグ
     */
    public completeEnableFlg: boolean;

    /**
     * 課題ワークアイテム
     */
    public workItem: WorkItem;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Issue {
        if (!json) {
            return this;
        }
        this.id = json['issue_id'];
        this.registrationDate = json['issue_registration_date'];
        this.number = json['issue_number'];
        this.subject = json['issue_subject'];
        this.detail = json['issue_detail'];
        this.correspondencePlanDate = json['issue_correspondence_plan_date'];
        this.correspondenceDate = json['issue_correspondence_date'];
        this.correspondenceDetail = json['issue_correspondence_detail'];
        this.completionDate = json['issue_completion_date'];
        this.exclusionNumber = json['issue_exclusion_number'];
        this.issueUrgency = new IssueUrgency().fromJson(json['issue_issue_urgency']);
        this.issueStatus = new IssueStatus().fromJson(json['issue_issue_status']);
        this.occurencePhase = new Phase().fromJson(json['issue_occurence_phase']);
        this.tosolvePhase = new Phase().fromJson(json['issue_tosolve_phase']);
        this.registeredPersonHumanResource = new HumanResource().fromJson(json['issue_registered_person_human_resource']);
        this.correspondingPersonHumanResource = new HumanResource().fromJson(json['issue_corresponding_person_human_resource']);
        this.optionItems = [];
        if (json['issue_option_items']) {
            json['issue_option_items'].forEach(
                item => {
                    this.optionItems.push(new OptionItem().fromJson(item));
                }
            );
        }
        this.relatedItems = [];
        if (json['issue_related_items']) {
            json['issue_related_items'].forEach(
                item => {
                    this.relatedItems.push(new WorkItem().fromJson(item));
                }
            );
        }
        this.attachments = [];
        if (json['issue_attachments']) {
            json['issue_attachments'].forEach(
                item => {
                    this.attachments.push(new Attachment().fromJson(item));
                }
            );
        }
        this.watcherHumanResources = [];
        if (json['issue_watcher_human_resources']) {
            json['issue_watcher_human_resources'].forEach(
                item => {
                    this.watcherHumanResources.push(new HumanResource().fromJson(item));
                }
            );
        }
        this.completeEnableFlg = json['issue_complete_enable_flg'];
        this.workItem = new WorkItem().fromJson(json['issue_work_item']);
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['issue_id'] = this.id;
        json['issue_registration_date'] = this.registrationDate;
        json['issue_number'] = this.number;
        json['issue_subject'] = this.subject;
        json['issue_detail'] = this.detail;
        json['issue_correspondence_plan_date'] = this.correspondencePlanDate;
        json['issue_correspondence_date'] = this.correspondenceDate;
        json['issue_correspondence_detail'] = this.correspondenceDetail;
        json['issue_completion_date'] = this.completionDate;
        json['issue_exclusion_number'] = this.exclusionNumber;
        json['issue_issue_urgency'] = this.issueUrgency ? this.issueUrgency.toJson() : {};
        json['issue_issue_status'] = this.issueStatus ? this.issueStatus.toJson() : {};
        json['issue_occurence_phase'] = this.occurencePhase ? this.occurencePhase.toJson() : {};
        json['issue_tosolve_phase'] = this.tosolvePhase ? this.tosolvePhase.toJson() : {};
        json['issue_registered_person_human_resource'] = this.registeredPersonHumanResource ? this.registeredPersonHumanResource.toJson() : {};
        json['issue_corresponding_person_human_resource'] = this.correspondingPersonHumanResource ? this.correspondingPersonHumanResource.toJson() : {};
        json['issue_option_items'] = [];
        if (this.optionItems) {
            this.optionItems.forEach(
                item => {
                    json['issue_option_items'].push(item.toJson());
                }
            );
        }
        json['issue_related_items'] = [];
        if (this.relatedItems) {
            this.relatedItems.forEach(
                item => {
                    json['issue_related_items'].push(item.toJson());
                }
            );
        }
        json['issue_attachments'] = [];
        if (this.attachments) {
            this.attachments.forEach(
                item => {
                    json['issue_attachments'].push(item.toJson());
                }
            );
        }
        json['issue_watcher_human_resources'] = [];
        if (this.watcherHumanResources) {
            this.watcherHumanResources.forEach(
                item => {
                    json['issue_watcher_human_resources'].push(item.toJson());
                }
            );
        }
        json['issue_complete_enable_flg'] = this.completeEnableFlg;
        json['issue_work_item'] = this.workItem ? this.workItem.toJson() : {};
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.registrationDate = null;
        this.number = null;
        this.subject = null;
        this.detail = null;
        this.correspondencePlanDate = null;
        this.correspondenceDate = null;
        this.correspondenceDetail = null;
        this.completionDate = null;
        this.exclusionNumber = null;
        this.issueUrgency = new IssueUrgency();
        this.issueStatus = new IssueStatus();
        this.occurencePhase = new Phase();
        this.tosolvePhase = new Phase();
        this.registeredPersonHumanResource = new HumanResource();
        this.correspondingPersonHumanResource = new HumanResource();
        this.optionItems = [];
        this.relatedItems = [];
        this.attachments = [];
        this.watcherHumanResources = [];
        this.completeEnableFlg = null;
    }

}