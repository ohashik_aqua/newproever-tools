
import { Model } from "../basis/model";
import { WorkItem } from './work-item';
import { ProgressStatus } from './progress-status';

/**
 * タスクを表します。
 */
export class Task implements Model {

    public static COLUMNS = ['task_id','task_scope_id','task_number','task_wbs_level1','task_wbs_level2','task_wbs_level3','task_wbs_level4','task_wbs_level5','task_pre_task_id','task_milestone','task_progress_status','task_start_plan_date','task_end_plan_date','task_plan_effort','task_overview','task_change_number','task_progress_rate',];

    /**
     * タスクID
     */
    public id: number;

    /**
     * タスクスコープID
     */
    public scopeId: number;

    /**
     * タスク番号
     */
    public number: number;

    /**
     * タスクワークアイテム
     */
    public workItem: WorkItem;

    /**
     * タスクWBSレベル1
     */
    public wbsLevel1: number;

    /**
     * タスクWBSレベル2
     */
    public wbsLevel2: number;

    /**
     * タスクWBSレベル3
     */
    public wbsLevel3: number;

    /**
     * タスクWBSレベル4
     */
    public wbsLevel4: number;

    /**
     * タスクWBSレベル5
     */
    public wbsLevel5: number;

    /**
     * タスク先行タスクID
     */
    public preTaskId: number;

    /**
     * タスクマイルストーン
     */
    public milestone: string;

    /**
     * タスク進捗ステータス
     */
    public progressStatus: ProgressStatus;

    /**
     * タスク開始予定日
     */
    public startPlanDate: Date;

    /**
     * タスク終了予定日
     */
    public endPlanDate: Date;

    /**
     * タスク予定工数
     */
    public planEffort: number;

    /**
     * タスク概要
     */
    public overview: string;

    /**
     * タスク変更番号
     */
    public changeNumber: string;

    /**
     * タスク進捗率
     */
    public progressRate: number;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): Task {
        if (!json) {
            return this;
        }
        this.id = json['task_id'];
        this.scopeId = json['task_scope_id'];
        this.number = json['task_number'];
        this.workItem = new WorkItem().fromJson(json['task_work_item']);
        this.wbsLevel1 = json['task_wbs_level1'];
        this.wbsLevel2 = json['task_wbs_level2'];
        this.wbsLevel3 = json['task_wbs_level3'];
        this.wbsLevel4 = json['task_wbs_level4'];
        this.wbsLevel5 = json['task_wbs_level5'];
        this.preTaskId = json['task_pre_task_id'];
        this.milestone = json['task_milestone'];
        this.progressStatus = new ProgressStatus().fromJson(json['task_progress_status']);
        this.startPlanDate = json['task_start_plan_date'];
        this.endPlanDate = json['task_end_plan_date'];
        this.planEffort = json['task_plan_effort'];
        this.overview = json['task_overview'];
        this.changeNumber = json['task_change_number'];
        this.progressRate = json['task_progress_rate'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['task_id'] = this.id;
        json['task_scope_id'] = this.scopeId;
        json['task_number'] = this.number;
        json['task_work_item'] = this.workItem ? this.workItem.toJson() : {};
        json['task_wbs_level1'] = this.wbsLevel1;
        json['task_wbs_level2'] = this.wbsLevel2;
        json['task_wbs_level3'] = this.wbsLevel3;
        json['task_wbs_level4'] = this.wbsLevel4;
        json['task_wbs_level5'] = this.wbsLevel5;
        json['task_pre_task_id'] = this.preTaskId;
        json['task_milestone'] = this.milestone;
        json['task_progress_status'] = this.progressStatus ? this.progressStatus.toJson() : {};
        json['task_start_plan_date'] = this.startPlanDate;
        json['task_end_plan_date'] = this.endPlanDate;
        json['task_plan_effort'] = this.planEffort;
        json['task_overview'] = this.overview;
        json['task_change_number'] = this.changeNumber;
        json['task_progress_rate'] = this.progressRate;
        return json;
    }
}