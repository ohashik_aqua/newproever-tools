
import { Model } from "../basis/model";
import { HumanResource } from './human-resource';

/**
 * ワークアイテムを表します。
 */
export class WorkItem implements Model {

    public static COLUMNS = ['work_item_id','work_item_name','work_item_start_implementation_date','work_item_end_implementation_date','work_item_owner_human_resource',];

    /**
     * ワークアイテムID
     */
    public id: number;

    /**
     * ワークアイテム名
     */
    public name: string;

    /**
     * ワークアイテム開始実施日
     */
    public startImplementationDate: Date;

    /**
     * ワークアイテム終了実施日
     */
    public endImplementationDate: Date;

    /**
     * ワークアイテム担当者ヒューマンリソース
     */
    public ownerHumanResource: HumanResource;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): WorkItem {
        if (!json) {
            return this;
        }
        this.id = json['work_item_id'];
        this.name = json['work_item_name'];
        this.startImplementationDate = json['work_item_start_implementation_date'];
        this.endImplementationDate = json['work_item_end_implementation_date'];
        this.ownerHumanResource = new HumanResource().fromJson(json['work_item_owner_human_resource']);
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['work_item_id'] = this.id;
        json['work_item_name'] = this.name;
        json['work_item_start_implementation_date'] = this.startImplementationDate;
        json['work_item_end_implementation_date'] = this.endImplementationDate;
        json['work_item_owner_human_resource'] = this.ownerHumanResource ? this.ownerHumanResource.toJson() : {};
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.name = null;
    }

}