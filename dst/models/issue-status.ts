
import { Model } from "../basis/model";

/**
 * 課題ステータスを表します。
 */
export class IssueStatus implements Model {

    public static COLUMNS = ['issue_status_id','issue_status_number','issue_status_name',];

    /**
     * 課題ステータスID
     */
    public id: number;

    /**
     * 課題ステータス番号
     */
    public number: number;

    /**
     * 課題ステータス名
     */
    public name: string;

    /**
     * JSONからこのオブジェクトを構築します。
     */
    public fromJson(json: object): IssueStatus {
        if (!json) {
            return this;
        }
        this.id = json['issue_status_id'];
        this.number = json['issue_status_number'];
        this.name = json['issue_status_name'];
        return this;
    }

    /**
     * このオブジェクトをJSONに変換します。
     */
    public toJson(): object {
        let json = {};
        json['issue_status_id'] = this.id;
        json['issue_status_number'] = this.number;
        json['issue_status_name'] = this.name;
        return json;
    }

    /**
     * このオブジェクトの値をクリアします。
     */
    public clear() {
        this.id = null;
        this.number = null;
        this.name = null;
    }

}