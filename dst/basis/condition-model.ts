export abstract class ConditionModel implements Model {

    public from: number;

    public to: number;

    abstract toJson(): object;

    abstract fromJson(json: object): Model;
}
