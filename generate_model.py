'''
モデルを表すクラスのソースを生成するツール。
'''
import codecs
import logging
import os
import re

import xlrd
from jinja2 import Environment, FileSystemLoader

LOGGER = logging.getLogger(__name__)

class Model:
    '''
    モデルを表します。
    '''
    name_snake = ''

    name_camel = ''

    name_upper_camel = ''

    name_kebab = ''

    description = ''

    items = []

    models = []

    is_condition = False

class Item:
    '''
    項目を表します。
    '''
    name_origin = ''

    name_snake = ''

    name_camel = ''

    description = ''

    type_name = ''

    length = 0

    is_array = False

    is_model = False

    is_displayed = False

def convert_snake_to_camel(snake):
    '''
    スネークケースからキャメルケースに変換します。
    '''
    return re.sub('_(.)', lambda x: x.group(1).upper(), snake)

def convert_snake_to_kebab(snake):
    '''
    スネークケースからケバブケースに変換します。
    '''
    return snake.replace('_', '-')

def convert_snake_to_upper_camel(snake):
    '''
    スネークケースからアッパーキャメルケースに変換します。
    '''
    camel = convert_snake_to_camel(snake)
    return camel[0].upper() + camel[1:]

def get_item_name(name, model_name):
    '''
    項目名を取得します。
    '''
    if name.find(model_name, 0, len(model_name)) > -1:
        tmp = name[len(model_name + '_'):]
        if tmp.startswith('_'):
            return tmp[1:]
        return tmp
    return name

def find_model(models, type_name):
    '''
    型の名称に紐づくモデルを取得します。
    '''
    for model in models:
        if model.name_snake == type_name:
            return model
    return None

def get_type_name(name):
    '''
    型の名称を取得します。
    '''
    length = 0
    matched = re.search(r'\(([0-9]+)\)', name)
    if matched:
        tmp = matched.group(0)
        LOGGER.debug(tmp)
        length = int(tmp[1:len(tmp)-1])
    if name.startswith('Date') or name.startswith('date'):
        return ('Date', length)
    if name.startswith('Numeric') or name.startswith('numeric'):
        return ('number', length)
    if name.startswith('Boolean') or name.startswith('boolean'):
        return ('boolean', length)
    if name.startswith('VarChar') or name.startswith('Char'):
        return ('string', length)
    return (name, length)

def read_definition():
    '''
    API仕様を読み込みます。
    '''
    src_path = os.path.dirname(__file__)
    doc_path = os.path.join(src_path, 'doc')
    models_all = []
    files_path = os.path.join(doc_path, 'API仕様')
    for file in os.listdir(files_path):
        LOGGER.debug(file)
        if not file.endswith('.xlsx') or file.startswith('~'):
            continue
        book = xlrd.open_workbook(os.path.join(files_path, file))
        sheet = book.sheet_by_name('モデル一覧')
        models = []
        for row in range(1, sheet.nrows):
            description = sheet.cell(row, 0).value
            if not description:
                break
            model = Model()
            models.append(model)
            model.description = description
            name_snake = sheet.cell(row, 1).value
            model.name_snake = name_snake
            model.name_camel = convert_snake_to_camel(name_snake)
            model.name_kebab = convert_snake_to_kebab(name_snake)
            model.name_upper_camel = convert_snake_to_upper_camel(name_snake)
            model.is_condition = name_snake.endswith('condition')
        models_all.extend(models)
        for model in models:
            LOGGER.debug(model.name_snake)
            model.items = []
            model.models = []
            try:
                sheet = book.sheet_by_name(model.name_snake)
                for row in range(1, sheet.nrows):
                    description = sheet.cell(row, 0).value
                    if not description:
                        break
                    item = Item()
                    model.items.append(item)
                    item.description = description
                    name_orign = sheet.cell(row, 1).value.lower()
                    item.name_origin = name_orign
                    name_snake = get_item_name(name_orign, model.name_snake)
                    item.name_snake = name_snake
                    item.name_camel = convert_snake_to_camel(name_snake)
                    type_name = sheet.cell(row, 2).value
                    item_model = find_model(models, type_name)
                    if item_model:
                        LOGGER.debug(item_model.name_snake)
                        item.type_name = item_model.name_upper_camel
                        item.is_model = True
                        if item_model.name_snake != model.name_snake:
                            has_import_model = False
                            for import_model in model.models:
                                if import_model.name_snake == item_model.name_snake:
                                    has_import_model = True
                                    break
                            if not has_import_model:
                                model.models.append(item_model)
                    else:
                        type_name_length = get_type_name(type_name)
                        item.type_name = type_name_length[0]
                        item.length = type_name_length[1]
                    item.is_array = bool(sheet.cell(row, 3).value)
                    item.is_displayed = bool(sheet.cell(row, 4).value)
            except:
                LOGGER.warning('no sheet.%s', model.name_snake)
                continue
    return models_all

def generate(models):
    '''
    ソースコードを生成します。
    '''
    src_path = os.path.dirname(__file__)
    dst_path = os.path.join(src_path, 'dst/models')
    LOGGER.debug('dst path:%s', dst_path)
    for dstf in os.listdir(dst_path):
        if os.path.isdir(os.path.join(dst_path, dstf)):
            continue
        os.remove(os.path.join(dst_path, dstf))
    env = Environment(loader=FileSystemLoader(os.path.join(src_path, 'templates'), encoding='utf-8'))
    template = env.get_template('model_template.txt')
    for model in models:
        if not model.items:
            continue
        body = template.render({'model': model})
        file_name = model.name_kebab + '.ts'
        with codecs.open(os.path.join(dst_path, file_name), 'w', encoding='utf-8') as dst:
            dst.write(body)

def main():
    '''
    API仕様を読み込み、モデルクラスを生成します。
    '''
    models = read_definition()
    LOGGER.debug('models.%s', len(models))
    generate(models)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    LOGGER.info('called.')
    main()
